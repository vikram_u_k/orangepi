Links and Collections for Protocol Support on OrangePi
https://github.com/igorpecovnik
https://github.com/lthiery/SPI-Py
http://linux-sunxi.org/Fex_Guide
http://linux-sunxi.org/Bootable_SPI_flash

How to enable SPI:
https://elixir.bootlin.com/linux/latest/source/tools/spi

mkdir /sys/kernel/config/device-tree/overlays/spi
cat spidev-enable.dtbo > /sys/kernel/config/device-tree/overlays/spi/dtbo


Run Commands using  https://forum.armbian.com/topic/3772-how-to-enable-hardware-spi/
./spidev_test -D /dev/spidev0.0 -s 10000 -b 8
./spidev-test -D /dev/spidev0.0 -s 10000 -p 123456789



Enable I2C
http://linux-sunxi.org/I2Cdev
